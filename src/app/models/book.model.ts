 /**
 * modelo para los libros que retorna el servicio
 */
export interface Book {
   /** id del libro*/
  id?: string;
   /** titulo del libro */
  titulo: string;
   /** autor del libro here */
  autor: string; 
  /** descripcion del libro */
  descripcion: string; 
  /** edicion prop here */
  edicion: string; 
  /** imagen prop here */
  imagen?: string;
  /** price prop here */
  price?: number; 
  /** isbn prop here */
  isbn?: number; 
  /** resumen prop here */
  resumen: string;
  /** serie prop here */
  serie: number; 
  /** tema prop here */
  tema:string;
   /** amount prop here */
  amount?:number;
   /** name prop here */
  name?:string; 
  
}
 /**
 * modelo para la respueta general 
 * @argument folio folio
 * @argument mensaje mensaje
 * @argument resultado resultado
 */
export interface ResponseInterface {
  /** Explain prop here */
  mensaje?: string;
  /** Explain prop here */
  folio: string;
  /** Explain prop here */
  resultado: Book[];

}


 /**
 * modelo para la respueta general 
 * @interface ResponseAddI
 */
export interface ResponseAddI {
  /** Explain prop here */
  mensaje?: string;
  /** Explain prop here */
  folio: string;
  /** Explain prop here */
  resultado:string;

}