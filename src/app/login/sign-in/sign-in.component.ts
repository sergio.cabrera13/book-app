import { Component, OnInit } from '@angular/core';
import { NgAuthService } from "../../services/auth.service";

/**
 * Sign In Component
 * componente para el login de la aplicación
 **/
@Component({
  //variable selector
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})

/**
 * Sign In Component de la claseSignInComponent
 * 
 **/
export class SignInComponent implements OnInit {
  /**
   * contructor de la clase SigIN
   * @param ngAuthService Variable de tipo auth
   * @constructor  inicio de la clase para garantizar el guardian
   **/
  constructor(

    /** 
     * ng On Init
     * @param ngAuthService Variable de tipo auth
     **/
    public ngAuthService: NgAuthService
  ) { }

  /** 
   * Metodo ng On Init sin implementar
   * @returns void
   **/
  ngOnInit() { }

}