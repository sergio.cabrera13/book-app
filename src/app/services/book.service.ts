import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book, ResponseInterface, ResponseAddI } from '../models/book.model';
import { environment } from '../../environments/environment.prod';
import swal from 'sweetalert2';
/**
 * Clase con los servicios del Api Rest
 * 
 */
@Injectable({
providedIn:"root"

})
export class BookService {
/**
 * Clase con los servicios del Api Rest
 * @constructor httclient
 */
  constructor(
    private readonly http: HttpClient
  ) { }


/**
 * servicio para obtener libros 
 * @returns Observable con la respuesta del servicio
 */
  public getBooks(): Observable<ResponseInterface> {
    const url: string = environment.API_REST_URL;
    const httpOptions = {
        headers: new HttpHeaders({
        uuid: '889898989'
      })
    };
    const Toast = swal.mixin({
      toast: true,
      position: 'bottom-end',
      showConfirmButton: false,
      timer: 2000,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer);
        toast.addEventListener('mouseleave', swal.resumeTimer);
      }
    });

    Toast.fire({
      icon: 'success',
      title:' libros disponibles'
    });
    console.info("get books");
    return this.http.get<ResponseInterface>(url,httpOptions);
  }
/**
 * servicio para obtener libros 
 * @returns Books[]: Observable con la respuesta del servicio
 */
  public getBooksFromCart(): Book[] {
    let listBook: Book[] = JSON.parse(localStorage.getItem('listCartBook'));
    if (listBook === null) {
      listBook = [];
    }
    return listBook;
  }
/**
 * servicio para obtener libros 
 * @returns Observable con la respuesta del servicio
 */
  public removeBooksFromCart(): void {
    localStorage.setItem('listCartBook', null);
  }
/**
 * servicio para agregar libros 
 * @returns Observable con la respuesta del servicio
 */
  public addBook(book: Book) {
    const url: string = environment.API_REST_URL;
    const httpOptions = {
        headers: new HttpHeaders({
        uuid: '123456'
      })
    };
    console.info("add books");
    return this.http.post<ResponseAddI>(url,book,httpOptions);
  }

/**
 * servicio para eliminar libros 
 * @param book string id del book
 * @returns Observable con la respuesta del servicio
 */
  public deleteBook(book: string) {
    const url: string = environment.API_REST_URL+"/"+book;
    
    const httpOptions = {
        headers: new HttpHeaders({
        uuid: '123456'
      })
    };
    console.info("add books");
    return this.http.delete<ResponseAddI>(url,httpOptions);
  }
/**
 * @todo servicio para actualizar libros 
 * sin implentantar
 * @returns Observable con la respuesta del servicio
 */
  public updateAmountBook(book: Book): Book[] {
    const listBookCart = this.getBooksFromCart();
    const index = listBookCart.findIndex((item: Book) => {
      return book.id === item.id;
    });
    if (index !== -1) {
      listBookCart[index].amount = book.amount;
      if (book.amount === 0) {
        listBookCart.splice(index, 1);
      }
    }
    localStorage.setItem('listCartBook', JSON.stringify(listBookCart));
    return listBookCart;
  }
/**
 * servicio para obtener libros 
 * @param book libro
 * @returns Observable con la respuesta del servicio
 */
  private _toastSuccess(book: Book) {
    const Toast = swal.mixin({
      toast: true,
      position: 'bottom-end',
      showConfirmButton: false,
      timer: 2000,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer);
        toast.addEventListener('mouseleave', swal.resumeTimer);
      }
    });
    Toast.fire({
      icon: 'success',
      title: book.name + ' added to cart'
    });
  }
}
