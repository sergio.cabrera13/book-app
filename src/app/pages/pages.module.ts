
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';

import { NavModule } from '../nav/nav.module';
import { ReduceTextPipe } from './reduce-text/reduce-text.pipe';
import { AddComponent } from './add/add.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    NavModule,
    PagesRoutingModule,
    ReactiveFormsModule, FormsModule,HttpClientModule
  ],
  declarations: [
    PagesComponent,
    HomeComponent,
    AddComponent,
    ReduceTextPipe,
  ],
  exports: [
    PagesComponent,NavModule,HomeComponent
  ]
})
export class PagesModule { }

