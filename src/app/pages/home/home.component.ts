import { Component, OnInit } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book, ResponseInterface, ResponseAddI } from '../../models/book.model';
import swal from 'sweetalert2';

  /** Explain prop here */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public listBook: Book[] = [];
  public data: ResponseInterface ;
  constructor(
    public readonly bookService: BookService
  ) { }

  ngOnInit(): void {

    this.getBooks();

  }
  /** Explain get here */
  public getBooks(): void {
    console.log(this.bookService.getBooks());

    this.bookService.getBooks().subscribe((resp: ResponseInterface) => {
      console.log(resp);
      this.data = resp;
      console.log(this.data.mensaje);
      this.listBook=this.data.resultado;
    });

  }

  /** Explain delete  here */
  onDelete(id:string){

    //implementar el Delete
    this.bookService.deleteBook(id).subscribe((resp: ResponseAddI) => {
    swal.fire({
      icon: 'success',
      title: 'Libro Eliminado correctamente!!'
    });
  });
  }

}
