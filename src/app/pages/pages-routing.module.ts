import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { AddComponent } from './add/add.component';
import { SignInComponent } from '../login/sign-in/sign-in.component';
import { SignUpComponent } from '../login/sign-up/sign-up.component';
import { DashboardComponent } from '../login/dashboard/dashboard.component';
import { ForgotPasswordComponent } from '../login/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from '../login/verify-email/verify-email.component';
import { AuthGuard } from '../auth.guard';


export const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: SignInComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'email-verification', component: VerifyEmailComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'home',    component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'add',    component: AddComponent , canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' }),
  ],
  exports: [
    RouterModule
  ]
})

export class PagesRoutingModule {}
