import { AddComponent } from './add.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BookService } from '../../services/book.service';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Book } from '../../models/book.model';

const listBook: Book[] = [
    {
        titulo: '',
        autor: '',
       descripcion:'',
        resumen: '',
        amount: 2,
        edicion:'',
        serie:0,
        tema:''
    },
    {
        titulo: '',
        autor: '',
       descripcion:'',
        resumen: '',
        amount: 2,
        edicion:'',
        serie:0,
        tema:''
    },
    {
        titulo: '',
        autor: '',
       descripcion:'',
        resumen: '',
        amount: 2,
        edicion:'',
        serie:0,
        tema:''
    }
];


describe('Cart component', () => {

    let component: AddComponent;
    let fixture: ComponentFixture<AddComponent>;
    let service: BookService;

    beforeEach( () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            declarations: [
                AddComponent
            ],
            providers: [
                BookService
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    beforeEach( () => {
        fixture = TestBed.createComponent(AddComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        service = fixture.debugElement.injector.get(BookService);
        spyOn(service, 'getBooksFromCart').and.callFake( () => listBook);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });


    it('getTotalPrice returns an amount', () => {
        const totalPrice = component.onClearBooks();
        expect(totalPrice).toBeGreaterThan(0);
        expect(totalPrice).not.toBeNull();
    });

    it('onInputNumberChange increments correctly', () => {
        const action = 'plus';
        const book = {
            titulo: '',
            autor: '',
           descripcion:'',
            resumen: '',
            amount: 2,
            edicion:'',
            serie:0,
            tema:''
        };

        const spy1 = spyOn(service, 'updateAmountBook').and.callFake( ()=> null);
        const spy2 = spyOn(component, 'onClearBooks').and.callFake( ()=> null);

        expect(book.amount).toBe(2);
        
        component.onInputNumberChange(action, book);

        expect(book.amount === 3).toBeTrue();

        expect(spy1).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();

    });    
    

    it('onInputNumberChange decrements correctly', () => {
        const action = 'minus';
        const book = {
            titulo: 'ss',
            autor: '',
           descripcion:'',
            resumen: '',
            amount: 2,
            edicion:'',
            serie:0,
            tema:''
        };

        expect(book.amount).toBe(3);

        const spy1 = spyOn(service, 'updateAmountBook').and.callFake( ()=> null);
        const spy2 = spyOn(component, 'onClearBooks').and.callFake( ()=> null);
        
        component.onInputNumberChange(action, book);

        expect(book.amount).toBe(2);

        expect(spy1).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();

    });

    it('onClearBooks works correctly', () => {
        const spy1 = spyOn((component as any), '_clearListCartBook').and.callThrough();
        const spy2 = spyOn(service, 'removeBooksFromCart').and.callFake( () => null);
        component.listCartBook = listBook;
        component.onClearBooks();
        expect(component.listCartBook.length).toBe(0);
        expect(spy1).toHaveBeenCalled();
        expect(spy2).toHaveBeenCalled();
    });

    it('_clearListCartBook works correctly', () => {
        const spy1 = spyOn(service, 'removeBooksFromCart').and.callFake( () => null);
        component.listCartBook = listBook;
        component["_clearListCartBook"]();

        expect(component.listCartBook.length).toBe(0);
        expect(spy1).toHaveBeenCalled();

    });

});