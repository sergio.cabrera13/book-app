import { Component, OnInit } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book, ResponseAddI } from '../../models/book.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';
/**
 * clase  para agregar libros  
 * @constant addBook addBook
 * @constant listCartBook listCartBook
 * @constructor cons
 */
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  public listCartBook: Book[] = [];
  public totalPrice = 0;
  public Math = Math;

  public addBook = new FormGroup({
    titulo: new FormControl('', Validators.required),
    autor: new FormControl('', Validators.required),
    isbn: new FormControl('', Validators.required),
    edicion: new FormControl('', Validators.required),
    imagen: new FormControl('', Validators.required),
    resumen: new FormControl('', Validators.required),
    serie: new FormControl('', Validators.required),
    tema: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required)
  })


  constructor(
    private readonly _bookService: BookService
  ) { }

  ngOnInit(): void {
  }

  onAddBook(form:Book) {
    console.log("form", form);
   if (form.titulo!=''&&form.autor!=''&&form.imagen!='') {
     
       this._bookService.addBook(form).subscribe((resp: ResponseAddI) => {
        console.log(resp);
        console.log(resp.mensaje);

        if (resp.resultado.length > 1) {
          swal.fire({
            icon: 'success',
            title: 'Libro ' + form.titulo + ' Agregado correctamente!!'
          });
        } else {
          swal.fire({
            icon: 'error',
            title: 'Libro ' + form.titulo + ' No se pudo agregar'
          });
       
        }
    
    });

  } else {
    swal.fire({
      icon: 'error',
      title: ' No se pudo agregar, Ingrese más información'
    });
  }
  }
  


  public onInputNumberChange(action: string, book: Book): void {
    const amount = action === 'plus' ? book.amount + 1 : book.amount - 1;
    book.amount = Number(amount);
    this.listCartBook = this._bookService.updateAmountBook(book);

  }

  public onClearBooks(): void {
    if (this.listCartBook && this.listCartBook.length > 0) {
      this._clearListCartBook();
    } else {
      console.log("No books available");
    }
  }

  private _clearListCartBook() {
    this.listCartBook = [];
    this._bookService.removeBooksFromCart();
  }


}
