/**
 * Constantes de ambientes de pruebas
 * @constant environment: contante
 */
export const environment = {

  production: false,
  API_REST_URL: 'https://us-central1-back-practica-final.cloudfunctions.net/app/api/libros',
  firebaseConfig :{
    apiKey: "AIzaSyCS8ewAApr8eV5epWb9KxN-utXKJyqyfwk",
    authDomain: "back-practica-final.firebaseapp.com",
    projectId: "back-practica-final",
    storageBucket: "back-practica-final.appspot.com",
    messagingSenderId: "322923213472",
    appId: "1:322923213472:web:eefd29d510714d8c320fc5",
    measurementId: "G-H0HPQM5041"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
